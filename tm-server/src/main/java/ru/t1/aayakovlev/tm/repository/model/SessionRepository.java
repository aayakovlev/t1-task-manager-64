package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends ExtendedRepository<Session> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<Session> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<Session> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @NotNull
    Optional<Session> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
