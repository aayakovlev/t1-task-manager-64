package ru.t1.aayakovlev.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataYamlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-yaml-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlLoadFasterXmlListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        domainEndpoint.yamlLoad(new DataYamlLoadFasterXmlRequest(getToken()));
    }

}
