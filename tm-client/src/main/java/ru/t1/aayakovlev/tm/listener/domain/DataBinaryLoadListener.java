package ru.t1.aayakovlev.tm.listener.domain;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataBinaryLoadRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-bin-load";

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataBinaryLoadListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD BINARY]");
        domainEndpoint.binaryLoad(new DataBinaryLoadRequest(getToken()));
    }

}
