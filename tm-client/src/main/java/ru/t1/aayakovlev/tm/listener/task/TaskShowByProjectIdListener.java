package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1.aayakovlev.tm.dto.response.TaskShowByProjectIdResponse;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Collections;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("Enter projectId: ");
        @NotNull final String projectId = nextLine();

        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskShowByProjectIdResponse response = taskEndpoint.showTasksByProjectId(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @NotNull final List<TaskDTO> tasks = response.getTasks();

        renderTasks(tasks);
    }

}
