package ru.t1.aayakovlev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

@Controller
@RequestMapping("/tasks")
public final class TaskController {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("")
    public ModelAndView index() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", repository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return new ModelAndView("task-list", "tasks", repository.findAll());
    }

    @GetMapping("/create")
    public String create() {
        repository.save(new Task("new Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        repository.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/edit/{id}")
    public String edit(
            @ModelAttribute("task") final Task task,
            final BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        repository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        final Task task = repository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectRepository.findAll());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
