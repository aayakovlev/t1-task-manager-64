package ru.t1.aayakovlev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

@Controller
@RequestMapping("/projects")
public final class ProjectController {

    @Autowired
    private ProjectRepository repository;

    @GetMapping("")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", repository.findAll());
    }

    @GetMapping("/create")
    public String create() {
        repository.save(new Project("new Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        repository.deleteById(id);
        return "redirect:/projects";
    }

    @PostMapping("/edit/{id}")
    public String edit(
            @ModelAttribute("project") final Project project,
            final BindingResult result
    ) {
        repository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        final Project project = repository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
